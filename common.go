package errorr

var (
	ErrNoAuthentication = NonRecovery(
		"there is no Authentication object found in request, remember to use authentication middleware",
	)

	ErrNotAuthenticated = NonRecovery(
		"request is not authenticated, remember to use authenticated middleware",
	)

	ErrWrongParamType = NonRecovery(
		"wrong param type",
	)

	ErrNoPrams = NonRecovery(
		"param with the specified key is not found",
	)
)
