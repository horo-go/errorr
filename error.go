package errorr

type BadRequestError struct {
	Err string
}

func (e *BadRequestError) Error() string {
	return e.Err
}

func BadRequest(err string) *BadRequestError {
	return &BadRequestError{err}
}

type NonRecoveryError struct {
	Err string
}

func (e *NonRecoveryError) Error() string {
	return e.Err
}

func NonRecovery(err string) *NonRecoveryError {
	return &NonRecoveryError{err}
}
